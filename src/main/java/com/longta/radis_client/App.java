package com.longta.radis_client;

import redis.clients.jedis.Jedis;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Jedis jedis = new Jedis("192.168.23.212");
        jedis.set("foo", "bar");
        String value = jedis.get("foo");
        jedis.close();
        System.out.println( "Hello World!" +value);
    }
}
