package com.longta.radis_client;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import redis.clients.jedis.Jedis;

public class RedisSetGetSample {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		
		Jedis jedis = new Jedis("192.168.23.212");
		
		int TEST_LOOP = 1000000;
		
		long start = System.currentTimeMillis();
		
		for(int i = 1; i <= TEST_LOOP; i++) {
			
			String res = jedis.set("key" + i, "valueaa" + i);
//			System.out.println(res);
		}
		
		long interval = (System.currentTimeMillis() - start)/1000;
		
		System.out.println("done " + TEST_LOOP + " SETs in " + interval + " seconds.");
		
//		start = System.currentTimeMillis();
//		
//		for(int i = 1; i <= TEST_LOOP; i++) {
//			String res = jedis.get("key" + i);		
////			System.out.println(res);
//		}
//		
//		long interval = (System.currentTimeMillis() - start) /1000;
//		
//		System.out.println("done " + TEST_LOOP + " GETs in " + interval + " seconds. ");

	}

}
